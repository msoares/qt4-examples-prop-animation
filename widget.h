#ifndef WIDGET_H
#define WIDGET_H

#include <QAbstractAnimation>
#include <QGraphicsItem>
#include <QGraphicsRectItem>
#include <QObject>
#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QTimer>
#include <QWidget>

class Widget: public QObject, public QGraphicsRectItem
{
    Q_OBJECT;
    Q_PROPERTY(QRectF rect READ rect WRITE setRect NOTIFY rectChanged);

public:
    Widget(QGraphicsItem *parent = 0);
    virtual ~Widget();

    void configureAnimation();

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

public slots:
    void onValueChanged(QVariant val);
    void onFinished();

signals:
    void rectChanged(QRectF);

protected:
    QAbstractAnimation *mAnimation;
    QTimer mTimer;
};

#endif /* end of include guard: WIDGET_H */
