#include <QColor>
#include <QBrush>
#include <QPen>
#include <QPropertyAnimation>

#include <QtDebug>

#include "widget.h"

Widget::Widget(QGraphicsItem *parent)
    : QObject(0)
    , QGraphicsRectItem(parent)
    , mAnimation(new QPropertyAnimation(this))
    , mTimer()
{
    setRect(0, 0, 1280, 720);
    setPen(QPen(QColor("black")));
    setBrush(QBrush(QColor("green")));

    mTimer.setInterval(2000);
    connect(&mTimer, SIGNAL(timeout()),
            mAnimation, SLOT(start()));

    connect(mAnimation, SIGNAL(valueChanged(QVariant)),
            this, SLOT(onValueChanged(QVariant)));

    connect(mAnimation, SIGNAL(finished()),
            this, SLOT(onFinished()));

    configureAnimation();
    mTimer.start();
}

Widget::~Widget()
{
    mTimer.stop();
    mAnimation->deleteLater();
}

void Widget::configureAnimation()
{
    QPropertyAnimation *anim = static_cast<QPropertyAnimation*>(mAnimation);

    anim->setTargetObject(this);
    anim->setDuration(400);

    anim->setPropertyName("rect");
    anim->setStartValue(QRectF(0, 0, 1280, 720));
    anim->setEndValue(QRectF(280, 72, 640, 360));
}

void Widget::onValueChanged(QVariant val)
{
    qDebug() << __PRETTY_FUNCTION__ << val;
}

void Widget::onFinished()
{
    QAbstractAnimation::Direction direction = mAnimation->direction();

    if (QAbstractAnimation::Forward == direction)
        mAnimation->setDirection(QAbstractAnimation::Backward);
    else
        mAnimation->setDirection(QAbstractAnimation::Forward);
}

QRectF Widget::boundingRect() const
{
    return rect();
}

void Widget::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    painter->setPen(pen());
    painter->setBrush(brush());
    painter->drawRect(rect());
}
