#include <QApplication>
#include <QGraphicsView>
#include <QGraphicsScene>

#include "widget.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QGraphicsScene scene;
    QGraphicsView view(&scene);
    Widget widget;

    scene.addItem(&widget);

    view.setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view.setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view.setContentsMargins(0, 0, 0, 0);
    view.setSceneRect(widget.sceneBoundingRect());
    view.centerOn(&widget);
    view.setFixedSize(1280, 720);
    view.setAttribute(Qt::WA_StyledBackground);
    view.setStyleSheet("background: black; border-style: none;");
    view.show();

    return app.exec();
}
